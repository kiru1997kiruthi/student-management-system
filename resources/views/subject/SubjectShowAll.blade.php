@extends('layout.home')
@section('content')
<div class="add-button">
    <a href="subject-add" class="btn btn-primary">Add New Subject</a>
    @if (\Session::has('success'))
        <div class="text-primary session-msg">
            <p>{{\Session::get('success')}}</p>
        </div>

        <script>
            $(function(){
                setTimeout(function(){
                    $('.session-msg').slideUp();
                },5000);
            });
        </script>
    @endif
</div>

<div class="table-layout">
    <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Subject Name</th>
            <th scope="col">Course Name</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($subjects as $subject)
            <tr>
                <td scope="row">{{$subject->id}}</td>
                <td>{{$subject->subject_name}}</td>
                <td>{{$subject->course_name}}</td>
                <td>
                    <a class="btn btn-warning" href="subject-edit/{{$subject->id}}">Edit</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="pagination">{{$subjects->links()}}</div>
@endsection